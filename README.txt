PyCraft Python server wrapper v0.9, by James Tidman

This is a wrapper, intended to make basic customisation of a server extremely easy. Please bear in mind, this wrapper essentially scripts standard commands to the minecraft console, so it has the limitations of those commands. If you're looking for a more in-depth (and harder to set up) custom server, you should look at mods such as Bukkit.

Updates to v0.9:
-Added a lot of the standard 'vanilla' server commands (!whitelist add/remove/on/off/reload/list, !op, !deop, !kick, !ban, !pardon, !save-all, !save-on, !save-off). This allows you to give some users control of some of these commands, without them being server ops.
-"!warp <player>" command added. Warps you to the location of that player.

Installation instructions:
1. Install and configure the standard server from Minecraft.net. Make sure you are using the .jar file and not the .exe. Bear in mind that the wrapper uses the standard server's whitelisting mechanism, so you'll need to enable whitelisting if you want to use it.
2. Unpack the files for the wrapper, and move them into the same directory as your Minecraft.jar file.
3. Edit the members.txt file to make yourself an owner. Do this by adding the line "yourname:5" (change "yourname" to your user name) to the end of the file.
4. If you want to customise which player levels can use which command, or spawn which block, edit the commands.txt and the blocklist.txt.
5. Put whatever messages you want in the help.txt, rules.txt and motd.txt. These will be read out by the console when somebody uses their respective commands. The contents of motd.txt will also be read out whenever a user logs in.
6. Make sure you have Python installed. If you don't, get it and install it from python.org
7. In Linux, use "python /path/to/wrapper.py" to start the wrapper. In windows, double-click on the "wrapper.py" icon, or type "C:\path\to\python.exe C:\path\to\wrapper.py" in the command prompt. The wrapper should start the server.
8. If you want to restart the server, use !restart (as owner). To shutdown, use !shutdown. To quit the wrapper, but keep the Minecraft server running (reverting it to a "vanilla" server), use !stopwrapper.
9. Use the !setgroup command to assign other players different user levels. This allows you to control which players can use which commands, as specified in the commands.txt file.
10. If you want to edit any of the settings in the .txt files without restarting the script, use the !reload command. Please note that this will NOT work for the main script (wrapper.py).

List of standard commands and what they do:
(In each case, the text in <> specifies additional options.)
!help - displays the text located in the help.txt file.
!rules - displays the text located in the help.txt file.
!motd - displays the text located in the help.txt file. This command is triggered automatically when a user logs in.
!give <blockid> <number> - gives you the specified number of the specified block. You can either use the standard data values (e.g. 1 is the data value for smooth stone), or the text name of the block, which is specified in blockalias.txt. If you do not specify a number, the default is 1 block. You can control which user groups can spawn which blocks by editing the blocklist.txt. EXAMPLE USAGE: !give cobblestone 128 
!reload - reloads all the resources that the wrapper uses. Use this command if you have changed the text files controlling command and block permissions, and don't want to restart the server.
!stopwrapper - this terminates the server wrapper without stopping the base server. After this, commands starting with ! will not work until you restart the wrapper and the server. You should not allow non-admins to use this command.
!shutdown - terminates the wrapper and the server. You should not allow non-admins to use this command.
!restart - restarts the server while keeping the wrapper running. You should not allow non-admins to use this command.
!list, !who - displays a list of the players currently online.
!setgroup <player> <group> - changes the user group of the player. EXAMPLE USAGE: "!setgroup johnsmith 3" would put the user "johnsmith" in the group 3.
!kit <kitname> - spawns a kit of items, as defined in kitlist.txt. EXAMPLE USAGE: "!kit basics" would spawn the basics kit.
!day - sets the time to just after sunrise, instantly making it daytime.
!warp <player> - warps you to the player's location. EXAMPLE USAGE: "!warp johnsmith" would warp you to johnsmith
!whitelist on/off - enables/disables whitelisting.
!whitelist add/remove <player> - whitelists or unwhitelists the player.
!whitelist list - lists all currently whitelisted players.
!whitelist reload - reloads the whitelist from white-list.txt.
!op <player> - makes the player an op. Please note, this makes them an op of the standard server, letting them use all server commands.
!deop <player> - removes the player from the op list.
!ban <player> - bans the player, provided their user group is lower than that of the user of the command.
!pardon <player> - unbans the player.
!kick - kicks the player, provided their user group is lower than that of the user of the command.
!save-all - forces a server-wide save.
!save-off - turns off automatic saving
!save-on - turns on automatic saving

If you have any suggestions for new features, or you've added your own features to this wrapper, please let me know at jamestidman@gmail.com.

Enjoy!
