

class RingBuffer(object):
    """ class that implements a not-yet-full buffer """
    def __init__(self, size_max):
        self.max = size_max
        self.data = []
        self.full = False
        self.cur = 0

    class __Full(object):
        """ class that implements a full buffer """
        def append(self, x):
            """ Append an element overwriting the oldest one. """
            self.data[self.cur] = x
            self.cur = (self.cur+1) % self.max

        def tolist(self):
            """ return list of elements in correct order. """
            return self.data[self.cur:] + self.data[:self.cur]

        def oldest(self):
            """ Return the oldest element from the list."""
            return self.data[self.cur:][0]

        def newest(self):
            """ Return the oldest element from the list."""
            return self.data[:self.cur][-1]

    def append(self, x):
        """ append an element at the end of the buffer. """
        self.data.append(x)
        if len(self.data) == self.max:
            self.cur = 0
            # Permanently change self's class from non-full to full
            self.__class__ = self.__Full
            self.full = True

    def tolist(self):
        """ Return a list of elements from the oldest to the newest. """
        return self.data

    def oldest(self):
        """ Return the oldest element from the list."""
        if len(self.data) >= 0:
            return self.data[0]
        else:
            return False

    def newest(self):
        """ Return the oldest element from the list."""
        if len(self.data) >= 0:
            return self.data[-1]
        else:
            return False
