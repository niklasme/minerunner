import json


def read_config(config_file):
    """
    Helper function to read json encoded config files

    :param config_file: Expects a pathlib.Path object as file descriptor
    :return: config: Content of json data, assumed to be dictionary but not required
    """

    try:

        with open(config_file.resolve(), 'r') as config_file_handle:
            config = json.loads(config_file_handle)
        return config
    except json.JSONDecodeError as err:
        print(f' ERROR: JSON member file could not be read')
        print(f' \t msg:  {err.msg}')
        print(f' \t doc:  {err.doc}')
        print(f' \t pos:  {err.pos}')
    except IOError:
        print(f'ERROR: File could not be opened: \n\t {config_file.resolve()}')
        raise


def read_text(text_file):
    """
    Read text files and remove comment lines
    :param text_file: Expects a pathlib.Paht object as file descriptor
    :return: text_array containing each line as an element in the array
    """

    try:
        with open(text_file.resolve(), 'r') as text_file_handle:
            text_array = []
            for line in text_file_handle:
                # Remove comments starting with #
                if line.startswith('#'):
                    pass
                else:
                    text_array.append(line.strip())
        return text_array

    except IOError:
        print(f'ERROR: File could not be opened: \n\t {text_file.resolve()}')
        raise


