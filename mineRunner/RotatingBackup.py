import sys
import os
import datetime
import zipfile
from mineRunner.RingBuffer import RingBuffer


class RotatingBackup:

    def __init__(self, backup_source_path=None, backup_target_path=None, number_of_files_to_store=20):

        try:
            # Set variables and read previous backups
            self.backupSourcePath = backup_source_path
            self.backupTargetPath = backup_target_path
            self.numberOfFilesToStore = number_of_files_to_store
            self.RB = RingBuffer(number_of_files_to_store)
            self.__read_backup_files()

            if not os.path.isdir(self.backupSourcePath):
                sys.exit(" \t The selected backup SOURCE path was not found")
            if not os.path.isdir(self.backupTargetPath):
                sys.exit(" \t The selected backup TARGET path was not found")
        except:
            error_type, error_msg = sys.exc_info()[0:2]

            print(f'ERROR: The rotatingBackup class could not be initiated correctly. \
                    \n\t\t {error_type} \n\t\t {error_msg}')

    # -----------------------------------------------------------------------------------
    def latest_backup(self):
        return self.RB.newest()

    # -----------------------------------------------------------------------------------
    def all_backups(self):
        return self.RB.tolist()

    # -----------------------------------------------------------------------------------
    def make_backup(self):
        try:
            # Create a new backup and remove the last backup in the rotating list
            new_backup_file_name = os.path.join(self.backupTargetPath, self.__time_to_str() + '_MC.zip')

            print("Backing up to file:\n\t %s " % new_backup_file_name)
            if not self.__make_zipfile(new_backup_file_name, self.backupSourcePath):
                sys.exit('ERROR: The compression failed at time stamp:  %s') % __timeToStr
            else:
                # Check if RingBuffer is full
                if self.RB.full:
                    # Get the oldest item in the list for removal
                    oldest_backup = os.path.join(self.backupTargetPath, self.RB.oldest())
                    try:
                        print("Remove old file:\n %s" % oldest_backup)
                        os.remove(oldest_backup)
                    except:
                        print("WARNING: Oldest backup file could not be removed:\n\t\t %s" % oldest_backup)

                # Append the latest backup to the rotating list
                self.RB.append(new_backup_file_name)
                print("  -Backing done!")
        except:
            print("ERROR: The rotatingBackup class could perform makeBackup command correctly. \n\t\t %s \n\t\t %s" %
                  (sys.exc_info()[0], sys.exc_info()[1]))

    def __time_to_str(self):
        return datetime.datetime.today().strftime("%Y-%m-%d_%H-%M-%S")

    def __make_zipfile(self, output_filename, source_dir):
        # Compress folder to zip-archive
        try:
            relroot = os.path.abspath(os.path.join(source_dir, os.pardir))
            with zipfile.ZipFile(output_filename, "w", zipfile.ZIP_DEFLATED) as zip:
                for root, dirs, files in os.walk(source_dir):
                    # add directory (needed for empty dirs)
                    zip.write(root, os.path.relpath(root, relroot))
                    for file in files:
                        filename = os.path.join(root, file)
                        if os.path.isfile(filename):  # regular files only
                            arcname = os.path.join(os.path.relpath(root, relroot), file)
                            zip.write(filename, arcname)
            return True
        except:
            print("ERROR: The folder compression failed!)")
            print(f'      Source folder: {source_dir}')
            print(f'      Target file:   {output_filename}')
            print('\n')

            print(f'       ERROR TYPE: {sys.exc_info()[0]}')
            print(f'       ERROR MSG: \n\t {sys.exc_info()[1]}')
            
            if zip.fh:
                # Close file if not closed
                zip.fh.close()
            return False

    def __read_backup_files(self):
        backups = self.__get_files_by_date_and_extension(self.backupTargetPath, 'zip')
        if len(backups) > 0:
            if len(backups) > self.numberOfFilesToStore:
                for file in backups[-self.numberOfFilesToStore:]:
                    self.RB.append(file)
            else:
                for file in backups:
                    self.RB.append(file)

    @staticmethod
    def __get_files_by_date_and_extension(self, dir_path, ext):
        a = [s for s in os.listdir(dir_path)
             if (os.path.isfile(os.path.join(dir_path, s))) and (s.endswith(ext))]
        a.sort(key=lambda s: os.path.getmtime(os.path.join(dir_path, s)))
        return a
    # -----------------------------------------------------------------------------------
