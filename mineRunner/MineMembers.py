# Updates the members.json file.

import json
from pathlib import Path


class MineMembers:

    def __init__(self):
        __this_path__ = Path(MineMembers.__file__).cwd()
        self.config_file = Path(__this_path__, 'config', 'members.json')
        self.members = {}

    def __read_config_file__(self):
        try:
            with open(self.config_file.resolve(), 'r') as config_filehandle:
                self.members = json.loads(config_filehandle)
        except json.JSONDecodeError as err:
            print(f' ERROR: JSON member file could not be read')
            print(f' \t msg:  {err.msg}')
            print(f' \t doc:  {err.doc}')
            print(f' \t pos:  {err.pos}')
        except:
            raise

    def __write_config_file__(self):
        try:
            members_json = json.dumps(self.config_file.resolve(), sort_keys=True, indent=4)
            with open(self.config_file.resolve(), 'w') as config_filehandle:
                config_filehandle.write(members_json)
        except:
            raise

    def add_member(self, member_name, level=1):
        try:
            if member_name in self.members:
                print(f' Player "{member_name}" is already a member of level {self.members[member_name]}')
                return False
            else:
                self.members[member_name] = level
                self.__write_config_file__()
                return True
        except:
            raise

    def set_level(self, member_name, level):
        if member_name in self.members:
            print(f' Player "{member_name}" level changed from {self.members[member_name]} to {level}')
            self.members[member_name] = level
            self.__write_config_file__()
