# Minecraft wapper extended by Niklas Melin and is built on code by James Tideman
#
# This wrapper was developed and tested on Python version 3.6.8
#
# Extended wrapper:
#     Version log:
#     0.7 - Implemented a rotating backup
#     0.6 - Fixed !list and !give command
#    <0.5 - Basic functionality for non-blocking stdout read implementation
#     
# Original Wrapper:
#    Minecraft server wrapper v0.9
#    By James Tidman
#     

# Import required modules
from mineRunner import config
from mineRunner import RotatingBackup
from mineRunner.read_config import read_config, read_text
import subprocess
import time
import sys
import os
import queue
import threading
from pathlib import Path


# ======================================================================================================================
# Class definitions
# ======================================================================================================================
class MineCraftWrapper:
    """
    Main module of Minecraft Wrapper
    """
    def __init__(self):
        self.debug = False
        # Load settngings from files
        self.__loadSettings__()
        self.defaultlevel = 1

        # Requred system paths
        # Path to the Java executable
        self.javaPath = config['system']['java-path']
        
        # Set running behavior and paths
        # Path to the Minecraft jar file
        self.minecraft_jar_path = config['minecraft']['jar-file']
        # Maximum memeory allocated by the server, in Mega Bytes
        self.maxMem=config['minecraft']['max-mem']
        # Minimum memeory allocated by the server, in Mega Bytes
        self.minMem=config['minecraft']['max-mem']
        # Path to where Minecraft is to be executed
        self.minecraftCwd=config['minecraft']['start-path']
        # Where Minecraft server stores the current world data
        self.worldDirectory=os.path.join(self.minecraftCwd,'world')

        # Path to storage point for backups
        self.minecraftWorldBackup=config['backup']['minecraft_world_backup_path']
        # Restart interval and backup interval of the server
        self.restartInterval=config['backup']['minecraft_world_backup_path']
        # Backup interval of the server
        self.backupInterval=config['backup']['backup_interval']
        # Number of backups to keep before replacement
        self.numberOfBackupsToStore=config['backup']['number_of_backups_to_store']

        # Set server status
        self.online = True
        self.restart = False
        self.isRunning = False
        #   Array of player names currently online
        self.onlinePlayers = []
        #   Time at last backup
        self.lastBackupTime = time.time()
        #   Time at last restart of server
        self.lastRestartTime = time.time()
        #   Wrapper start time
        self.onLineTime = time.time()

        # Call instance of backup class
        self.rotatingBackup = RotatingBackup(backup_source_path=self.worldDirectory,
                                             backup_target_path=self.minecraftWorldBackup,
                                             number_of_files_to_store=self.numberOfBackupsToStore)

        # Set up threads for non-locking behavior
        #   Thread lock for avoiding reading and writing to the same variable
        self.queueLock = threading.Lock()
        #   Queue for writing progression log. Infinite number of items in the queue are accepted
        self.stdoutQueue = queue.Queue(0)

    # -----------------------------------------------------------------------------------
    def __loadSettings__(self):
        self.motd = read_text(Path('./config/welcome.txt'))
        self.rules = read_text(Path('./config/rules.txt'))
        self.members = read_config(Path('./config/members.json'))
        self.commands = read_config(Path('./config/commands.json'))
        self.kit_list = read_config(Path('./config/kitlist.json'))

    # -----------------------------------------------------------------------------------
    def __sendCmd__(self, cmd):
        self.server.stdin.write(cmd)

    # -----------------------------------------------------------------------------------
    def __readOutput__(self):
            try:
                self.queueLock.acquire()
                last_output_line = self.stdoutQueue.get(False, 0.1)
                self.stdoutQueue.task_done()
                self.queueLock.release()
                return last_output_line
            except:
                self.queueLock.release()
                # There was nothing new in the output
                return False
    # -----------------------------------------------------------------------------------
    def tell(self, text, player):
        #Whispers a list to a player, one element per line
        for line in text:
            self.server.stdin.write('tell %s %s \n' % (player, line))
    # -----------------------------------------------------------------------------------
    def say(self, text):
        #Whispers a list to a player, one element per line
        for line in text:
            self.server.stdin.write('say %s \n' % (line) )

    # -----------------------------------------------------------------------------------
    def startServer(self):
        startCommand='{javaPath} -Xmx{maxMem}M -Xms{minMem}M -jar {minecraft_jar_path} nogui -d64'.format( javaPath=self.javaPath , maxMem=self.maxMem , minMem=self.minMem , minecraft_jar_path=self.minecraft_jar_path )
        print("Starting server using command: \n\t{}".format(startCommand))
        self.server=subprocess.Popen(startCommand, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.minecraftCwd)
        time.sleep(5) # Wait for process to start
        if ( self.server.poll() == None ):
            self.isRunning=True
            self.lastRestartTime=time.time()
            # Start up stdout read thread
            self.stdoutThread = stdoutThread(self.server , self.queueLock , self.stdoutQueue, debug=self.debug)
            self.stdoutThread.deamon = True
            self.stdoutThread.start()
            print("Server online")
        else:
            print("ERROR: Failed to start server, waiting a while and then tries again")
            self.isRunning=False
            time.sleep(60)
            self.startServer()
    # -----------------------------------------------------------------------------------
    def stopServer(self):
        self.__sendCmd__('say NOTICE: Server is going down in 30 seconds\n')
        time.sleep(12)
        for i in range(20,0,-2):
            self.__sendCmd__('say NOTICE: Server is going down in %s seconds\n' %i)
            time.sleep(2)
        self.__sendCmd__('say NOTICE: Server is going down NOW!\n')
        time.sleep(2)
        self.__sendCmd__('save-all\n')
        self.__sendCmd__('stop\n')
        self.stdoutThread.close()
        self.stdoutThread.join(5)
        self.online=False
        try:
            self.rotatingBackup.make_backup()
        except:
            print("ERROR:Backup failed after server shutdown. \n\t\t %s \n\t\t %s" % (sys.exc_info()[0],  sys.exc_info()[1]))
    # -----------------------------------------------------------------------------------
    def restartServer(self):
        self.__sendCmd__('say NOTICE: A RESTART will be done in 2 minutes\n')
        time.sleep(120)
        for i in range(20,0,-2):
            self.__sendCmd__('say NOTICE: A RESTART will be done in %s seconds\n' %i)
            time.sleep(2)
        self.__sendCmd__('say NOTICE: Server is going down NOW!\n')
        time.sleep(2)
        self.__sendCmd__('save-all\n')
        self.__sendCmd__('stop\n')
        self.stdoutThread.close()
        try:
            self.rotatingBackup.make_backup()
        except:
            print("ERROR:Backup failed during server restart. \n\t\t %s \n\t\t %s" % (sys.exc_info()[0],  sys.exc_info()[1]))
        time.sleep(15)
        self.startServer()
        # Start up stdout read thread
        self.stdoutThread = stdoutThread(self.server , self.queueLock , self.stdoutQueue, debug=self.debug)
        self.stdoutThread.deamon = True
        self.stdoutThread.start()
        self.onlinePlayers=[]
    # -----------------------------------------------------------------------------------
    def runServer(self):
        """

        """
        print("Start server")
        self.startServer()
        while self.online:

            # Add some delay
            time.sleep(0.01)

            # Check if the server process is running
            if not (self.server.poll() == None):
                print("The server had unexpectidly closed/crashed, restarting")
                self.startServer()
                time.sleep(1)

            # Write server output to console
            self.lastOutputLine=self.__readOutput__()
            if self.lastOutputLine != False:
                print(self.lastOutputLine)

            # Check if it is lagging or overloaded
            #[18:05:05] [Server thread/WARN]: Can't keep up! Did the system time change, or is the server overloaded? Running 67279ms behind, skipping 1345 tick(s)
            #if not self.lastOutputLine == False:
            #    if ("[Server thread/WARN]:" == self.lastOutputLine.split(' ')[1] ) and ( "Can't keep up! Did the system time change, or is the server overloaded?" in self.lastOutputLine ):
            #        print("Server lagging behind, restarting"
            #        self.restartServer()

            # Check if time for scheduled reboot
            if time.time()-self.lastRestartTime > self.restartInterval:
                print(" It is time to restart the server")
                self.restartServer()

            # Check if time for scheduled backup
            if time.time()-self.lastBackupTime > self.backupInterval:
                print(" It is time to backup the server")
                self.backupServer()
                self.lastBackupTime=time.time()

            # Check wrapper functionality
            if not self.lastOutputLine == False:
                try:
                    self.checkCommand()
                except:
                    print("WARNING: An error occured while parsing commands from stdin")
                    print("\t\t Last line read was:\n\t\t %s" %self.lastOutputLine)
                    print(sys.exc_info()[0]) #,' - Exit type:', sys.exc_info()[0]
                    print(sys.exc_info()[1]) #,' - Exit type:', sys.exc_info()[0]

        if self.online:
            print("ERROR: Out of while loop")
    # -----------------------------------------------------------------------------------
    def backupServer(self):
        try:
            self.rotatingBackup.make_backup()
        except:
            print("ERROR:Backup failed after server shutdown. \n\t\t %s \n\t\t %s" % (sys.exc_info()[0], sys.exc_info()[1]))
    # -----------------------------------------------------------------------------------
    def haspermission(self,player, command, v=1):
        #Checks whether the player has permission to use a command.
        if not self.members.has_key( player ):
            print("Player %s not in members" % player)
            playerlevel = self.defaultlevel
        else:
            playerlevel = int(self.members[player])
        if (playerlevel < int(self.commands[command])):
            if (v==1):
                self.server.stdin.write('tell %s Your user level needs to be at least %s to use that command. Currently your user level is %s.\n' % (player, str(self.commands[command]), str(playerlevel)))
            return False
        else:
            return True
    # -----------------------------------------------------------------------------------
    def checkCommand(self):
        words = self.lastOutputLine.split()

        #Commands are listed below

        #Sets the game time to 0 (just after sunrise). If the player is alone in the server, uses the permissions for 'day'. Otherwise, uses the permissions for 'forceday'.
        if ('!backup' in words):
            player = words[3].strip('<>')
            if (self.haspermission(player,'backup')==True):
                self.server.stdin.write('tell '+player+' Performing backup\n')
                self.backupServer()
                self.server.stdin.write('tell '+player+'   Backup complete\n')
            else:
                self.server.stdin.write('tell '+player+' You do not have permission to perform this command.\n')

        #Lists all online players
        if('!list' in words):
            if(words.index('!list')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'list')==True):
                    self.__sendCmd__('list\n')
                    time.sleep(1)
                    onlineplayers = self.__readOutput__()
                    if not onlineplayers==False:
                        #[21:54:55] [Server thread/INFO]: There are 2/20 players online:
                        try:
                            print(f' Online players: {onlineplayers}')
                            numberOfOnlinePlayers= int(onlineplayers.split(' ')[5].split('/')[0])
                            print(f'Total: {numberOfOnlinePlayers}')
                            temp=[f'There are currently {numberOfOnlinePlayers} players online:\n']
                            for i in range(0,numberOfOnlinePlayers):
                                output=self.__readOutput__()
                                if not output == False:
                                    temp.append( output.split(':')[-1]+'\n' )
                                    time.sleep(0.05)
                            print(temp)
                            self.tell(temp,player)
                        except:
                            print("!LIST ERROR")
                            print(sys.exc_info()[0]) #,' - Exit type:', sys.exc_info()[0]
                            print(sys.exc_info()[1]) #,' - Exit type:', sys.exc_info()[0]


        #Gives the player some of the block specified, provided they are in a high enough group to do so.
        if('!give' in words):
            if(words.index('!give')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'give')==True):
                    if(len(words) == 6)or (len(words) == 7):
                        item = words[5]
                        if (len(words)==6):
                            num = 1
                        elif (len(words)==7):
                            num = int(words[6])

                        if (num>64):
                            self.server.stdin.write('tell '+player+' You cannot spawn more than 64 of any block at a time.\n')
                        else:
                            if item.startswith('minecraft:'):
                                self.server.stdin.write('give %s %s %s\n' % (player, item, num))
                            else:
                                self.server.stdin.write('give %s minecraft:%s %s\n' % (player, item, num))
                    else:
                        self.server.stdin.write('tell '+player+' Error: syntax is !give <item> <num>.\n')
                else:
                    self.server.stdin.write('tell '+player+' You do not have permission to spawn that item at your level.\n')

        #Whispers the contents of the 'help.txt' file.
        if('!help' in words):
            if(words.index('!help')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'help')==True):
                    self.tell(self.help, player)

        #Whispers the contents of the 'welcome.txt file.
        if('!motd' in words):
            if(words.index('!motd')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'motd')==True):
                    self.tell(self.motd, player)

        #Whispers the contents of the 'rules.txt' file.
        if('!rules' in words):
            if(words.index('!rules')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'rules')==True):
                    self.tell(self.rules, player)

         #Gives the player the items specified in the kitlist.json file.
         #if('!kit' in words):
         #    if(words.index('!kit')==4):
         #        player = words[3].strip('<>')
         #        if (self.haspermission(player,'kit')==True):
         #            if (len(words)==5):
         #                self.server.stdin.write('tell '+player+' Error: syntax is !kit <kitname>.\n')
         #            else:
         #                kit = words[5]
         #                if (kit not in self.kitlist):
         #                    self.server.stdin.write('tell '+player+' Error: You specified a kit that does not exist.\n')
         #                else:
         #                    kits=self.kitlist[kit].split('*')
         #                    if (kits[0]>self.members[player.lower()]):
         #                        self.server.stdin.write('tell '+player+' You do not have permission to use that kit.\n')
         #                    else:
         #                        kits = kits[1].split(',')
         #                        for item in kits:
         #                            x = item.split(':')
         #                            self.server.stdin.write('give %s %s %s\n' % (player, x[0], x[1]))

        #Sets the game time to 0 (just after sunrise). If the player is alone in the server, uses the permissions for 'day'. Otherwise, uses the permissions for 'forceday'.
        if ('!day' in words):
            if(words.index('!day')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'forceday', 0)==True):
                    self.server.stdin.write('time set 0\n')
                elif (self.haspermission(player,'day')==True) and (onlinenumber==1):
                    self.server.stdin.write('time set 0\n')

        #Warps the player to the location of another player.
        if('!warp' in words):
            player = words[3].strip('<>')
            if(words.index('!warp')==4)and(len(words)==6):
                if (self.haspermission(player,'warp')==True):
                    self.server.stdin.write('tp %s %s \n' % (player, words[5].strip('<>')))

        #Puts the target player in the indicated user group. Cannot be used to set the group of a player in the same of higher group as the player.
        if ('!setgroup' in words):
            if(words.index('!setgroup')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'setgroup')==True):
                    if (len(words)==7):
                        target = words[5]
                        groupid = words[6]
                        try:
                            int(groupid)
                            if (target.lower() not in self.members):
                                self.server.stdin.write('tell %s Cannot find the user %s.\n' % (user, target))
                            elif(self.members[target.lower()]>=self.members[user.lower()]):
                                self.server.stdin.write('tell '+user+' You cannot edit the group id of a player with the same group or higher.\n')
                            elif(groupid>=self.members[user.lower()]):
                                self.server.stdin.write('tell '+user+' You cannot give another player your group id or higher.\n')
                            else:
                                self.members[target.lower()] = groupid
                                writemembers(self.members)
                                self.server.stdin.write('tell %s %s has been assigned a group id of %s.\n' % (user, target, groupid))
                                self.server.stdin.write('tell %s You have been assigned a group id of %s by a moderator.\n' % (target, groupid))
                        except Exception:
                            self.server.stdin.write('tell '+user+' Error: syntax is "!setgroup <player> <groupid>"\n')    
                    else:
                        self.server.stdin.write('tell '+player+' Error: syntax is !setgroup <player> <groupid>.\n')

        #When a user logs in, whispers the contents of 'welcome.txt' to them. If the user is new, sets a default group and adds them to 'members.json'.
        if ( 'joined the game' in self.lastOutputLine ):

            try:
                #if ( self.lastOutputLine.split(':')[0].endswith("[Server thread/INFO]") ) and ( 'joined the game' in self.lastOutputLine ):
                print(" Player joined")
                newplayer = words[3].split('[')[0].strip()
                if (newplayer not in self.members):
                    self.members[newplayer] = self.defaultlevel
                    writemembers(self.members)
                self.tell(self.motd, newplayer)
                print("Append to onlinePlayers")
                self.onlinePlayers.append(newplayer)
                self.say(['The following %s players are on line' %len(self.onlinePlayers)]+self.onlinePlayers)
                print("Player appended to onlinePlayers")
            except:
                print("ERROR IN Join the game")

        #Updates the total number of players online when a user quits. This is used for the !day command.
        #[18:12:17] [Server thread/INFO]: com.mojang.authlib.GameProfile@12af580c[id=<null>,name=Willet2006,properties={},legacy=false] (/178.174.219.241:49415) lost connection: Disconnected properties={},legacy=false] (/178.174.219.241:49417) lost connection: Disconnected
        #[16:58:29] [Server thread/INFO]: AngryFinus lost connection: TextComponent{text='Disconnected', siblings=[], style=Style{hasParent=false, color=null, bold=null, italic=null, underlined=null, obfuscated=null, clickEvent=null, hoverEvent=null, insertion=null}}
        #[16:48:54] [Server thread/INFO]: Bowlingmaster1 left the game

        #if('[Server thread/INFO]:' in words) and ('' in words):#This needs to be fixed, since it can be triggered by a say command.
        if ( 'left the game' in self.lastOutputLine ):
            leavingPlayer=words[3].strip()
            try:
                pos=self.onlinePlayers.index(leavingPlayer)
                self.onlinePlayers.pop(pos)
                self.say(['There are %i players left on the server' %len(self.onlinePlayers) ])
            except:
                print("Player %s left the game, but was not in the online list" % leavingPlayer)
                print(sys.exc_info()[0]) #,' - Exit type:', sys.exc_info()[0]
                print(sys.exc_info()[1]) #,' - Exit type:', sys.exc_info()[0]

        #Reloads the config files that this wrapper uses, for example 'commands.json', without restarting the server.
        if('!reload' in words):
            if(words.index('!reload')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'reload')==True):
                    self.__loadSettings__()
                    self.server.stdin.write('tell '+player+' Server resources successfully reloaded.\n')

        #Stops the server and the wrapper.
        if('!shutdown' in words):
            if(words.index('!shutdown')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'shutdown')==True):
                    self.stopServer()

        #Restarts the server, but leaves the wrapper running.
        if('!restart' in words):
            if(words.index('!restart')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'restart')==True):
                    self.restartServer()

        #Assigns the player op status. Please note, this makes the person an op of the 'vanilla' server, and gives them use of the commands starting with '/'.
        if('!op' in words):
            player = words[3].strip('<>')
            if(words.index('!op')==4)and(len(words)==6):
                target = words[5].lower()
                if (self.haspermission(player,'op')==True):
                    if (target not in self.members):
                        self.server.stdin.write('tell %s Cannot find the user %s.\n' % (player, target))
                    else:
                        self.server.stdin.write('op '+target+'\n')

        #Removes op status from the target player, as long as that player is in a lower group.
        if ('!deop' in words):
            if(words.index('!deop')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'deop')==True):
                    if (len(words)==6):
                        target = words[5].lower()
                        if (target not in self.members):
                            self.server.stdin.write('tell %s Cannot find the user %s.\n' % (player, target))
                        elif(self.members[target]>=self.members[player.lower()]):
                            self.server.stdin.write('tell '+player+' You cannot deop a player with the same group or higher.\n')
                        else:
                            self.server.stdin.write('deop '+target+'\n')                            
                    else:
                        self.server.stdin.write('tell '+player+' Error: syntax is !deop <player>.\n')

        #Bans the target player, as long as that player is in a lower group.
        if ('!ban' in words):
            if(words.index('!ban')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'ban')==True):
                    if (len(words)==6):
                        target = words[5].lower()
                        if (target not in self.members):
                            self.server.stdin.write('tell %s Cannot find the user %s.\n' % (player, target))
                        elif(self.members[target]>=self.members[player.lower()]):
                            self.server.stdin.write('tell '+player+' You cannot ban a player with the same group or higher.\n')
                        else:
                            self.server.stdin.write('ban '+target+'\n')                            
                    else:
                        self.server.stdin.write('tell '+player+' Error: syntax is !ban <player>.\n')

        #Unbans the target player.
        if('!pardon' in words):
            player = words[3].strip('<>')
            if(words.index('!pardon')==4)and(len(words)==6):
                target = words[5].lower()
                if (self.haspermission(player,'pardon')==True):
                    if (target not in self.members):
                        self.server.stdin.write('tell %s Cannot find the user %s.\n' % (player, target))
                    else:
                        self.server.stdin.write('pardon '+target+'\n')

        #Kicks the target player, as long as that player is in a lower group.
        if ('!kick' in words):
            if(words.index('!kick')==4):
                player = words[3].strip('<>')
                if (self.haspermission(player,'kick')==True):
                    if (len(words)==6):
                        target = words[5].lower()
                        if (target not in self.members):
                            self.server.stdin.write('tell %s Cannot find the user %s.\n' % (player, target))
                        elif(self.members[target]>=self.members[player.lower()]):
                            self.server.stdin.write('tell '+player+' You cannot kick a player with the same group or higher.\n')
                        else:
                            self.server.stdin.write('kick '+target+'\n')                            
                    else:
                        self.server.stdin.write('tell '+player+' Error: syntax is !kick <player>.\n')

        #Forces a full save.
        if('!save-all' in words):
            player = words[3].strip('<>')
            if(words.index('!save-all')==4):
                if (self.haspermission(player,'save-all')==True):
                    self.server.stdin.write('save-all\n')

        #Turns off automatic saving
        if('!save-off' in words):
            player = words[3].strip('<>')
            if(words.index('!save-off')==4):
                if (self.haspermission(player,'save-off')==True):
                    self.server.stdin.write('save-off\n')

        #Turns on automatic saving
        if('!save-on' in words):
            player = words[3].strip('<>')
            if(words.index('!save-on')==4):
                if (self.haspermission(player,'save-on')==True):
                    self.server.stdin.write('save-on\n')

        #Kicks the target player, as long as that player is in a lower group.
        if ('!whitelist' in words):
            if(words.index('!whitelist')==4):
                player = words[3].strip('<>')
                if (len(words)==5):
                    self.server.stdin.write('tell '+player+' Error: syntax is !whitelist <command> <option>.\n')
                elif (len(words)==6):
                    if (words[5]=='list') and (self.haspermission(player,'whitelistlist')==True):
                        self.server.stdin.write('whitelist list\n')
                        whitelistplayers = p.stderr.readline().split()
                        try:
                            self.server.stdin.write('tell '+player+' Whitelisted players: ')
                            x=1
                            for words in whitelistplayers:
                                if (x <= 5):
                                    pass
                                else:
                                    self.server.stdin.write(words+' ')
                                x=x+1
                            self.server.stdin.write('\n')

                        except Exception:
                            pass
                        p.stderr.readline()
                    elif (words[5]=='reload') and (self.haspermission(player,'whitelistreload')==True):
                        self.server.stdin.write('whitelist reload\n')
                    elif (words[5]=='on') and (self.haspermission(player,'whiteliston')==True):
                        self.server.stdin.write('whitelist on\n')
                    elif (words[5]=='off') and (self.haspermission(player,'whitelistoff')==True):
                        self.server.stdin.write('whitelist off\n')
                    elif (words[5]=='add') or (words[5]=='remove'):
                        self.server.stdin.write('tell '+player+' Error: syntax is !whitelist <command> <option>.\n')
                    else:
                        self.server.stdin.write('tell '+player+' Error: command not found.\n')
                elif (len(words)==7):
                    if (words[5]=='add') and (self.haspermission(player,'whitelistadd')==True):
                        self.server.stdin.write('whitelist add '+words[6]+'\n')
                    elif (words[5]=='remove') and (self.haspermission(player,'whitelistremove')==True):
                        target = words[6].lower()
                        if (target in self.members) and (self.members[target]>=self.members[player.lower()]):
                            self.server.stdin.write('tell '+player+' You cannot unwhitelist a player with the same group or higher.\n')
                        else:
                            self.server.stdin.write('whitelist remove '+words[6]+'\n')
                    else:
                        self.server.stdin.write('tell '+player+' Error: command not found.\n')
                else:
                    self.server.stdin.write('tell '+player+' Error: syntax is !whitelist <command> <option>.\n')
# -----------------------------------------------------------------------------------------------------------------------------------------------------
class stdoutThread (threading.Thread):
    def __init__(self,server, queueLock,stdoutQueue,debug=False):
        threading.Thread.__init__(self)
        # Basic settings
        self.server=server
        self.stdout=self.server.stdout
        self.stdoutQueue=stdoutQueue
        self.queueLock=queueLock
        self.exitFlag=False

    def run(self):
        self.readStdOut()

    def readStdOut(self):
        # Read until the exit flag is set
        while ( not self.exitFlag ):
            if ( self.server.poll() == None ):
                time.sleep(0.05)
                read_line=self.stdout.readline().strip()
                self.queueLock.acquire()
                self.stdoutQueue.put(read_line)
                self.queueLock.release()
            else:
                print("WARNING: The server process closed unexpectidly. Terminateing stdoutProcess thread!")
                # Empty queue the server process is not available and exit thread. A new thread is generated when the server is restarted.
                time.sleep(1)
                self.queueLock.acquire()
                while not self.stdoutQueue.empty():
                    print(self.stdoutQueue.get())
                    self.stdoutQueue.task_done()
                self.queueLock.release()
                self.stdoutQueue.join()
                self.exitFlag=True
                print("WARNING: stdoutProcess, terminated!")

    def close(self):
        print("INFO: stdoutProcess, recieved close signal!")
        # Empty queue to be able to close the thread
        self.exitFlag=True
        time.sleep(1)
        while not self.stdoutQueue.empty():
            try:
                self.queueLock.acquire()
                print(self.stdoutQueue.get(False,0.1))
                self.stdoutQueue.task_done()
                self.queueLock.release()
            except:
                self.queueLock.release()
        self.stdoutQueue.join()
        print("INFO: stdoutProcess, stop completed!")
# =====================================================================================================================================================

# =====================================================================================================================================================
# M A I N
if __name__ == "__main__":
    MC=MinecraftWrapper()
    MC.runServer()
    sys.exit('The wrapper has terminated succesfully.')


